django-picklefield==0.3.2
django-constance==1.1.2
Django==1.9.2
psycopg2==2.6.1
django-tables2==1.1.1
