Установка
----------

Установка состоит из следующих этапов:
- установка системных пакетов (python 3.4+, python-venv, python-dev и тд)
- установка и настройка postgresql
- создания окружения и установка пакетов из requirements.txt (pip3 install -r requirements.txt) в окружении
- настройка парметров подключения в файле web/settings.py



Запуск
--------

На текущий момент продакшн версия состоит из компонент:
- база данных postgresql
- django runserver
- сервис для приема данных от трекеров


в отдельных сессиях tmux или screen запустить в созданном виртуальном окружении:
python manage.py runserver 0.0.0.0:80
python manage.py listen
