from django.core.management.base import BaseCommand, CommandError
from server.server import main

class Command(BaseCommand):
    help = 'Run tracker server'

    def handle(self, *args, **options):
        main()


