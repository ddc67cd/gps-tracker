import sys
import asyncio
import asyncio.streams
from server.packet import Packet
from data.models import Entry


class GPSServer:
    def __init__(self, host, port):
        self.server = None
        self.host = host
        self.port = port
        self.clients = {} # task -> (reader, writer)

    def _accept_client(self, client_reader, client_writer):
        """
        This method accepts a new client connection and creates a Task
        to handle this client.  self.clients is updated to keep track
        of the new client.
        """
        peername = client_writer.get_extra_info('peername')
        print('connection from {}'.format(peername))

        # start a new Task to handle this specific client connection
        task = asyncio.Task(self._handle_client(client_reader, client_writer))
        self.clients[task] = (client_reader, client_writer)

        def client_done(task):
            print("client task done:", task, file=sys.stderr)
            del self.clients[task]

        task.add_done_callback(client_done)

    @asyncio.coroutine
    def _handle_client(self, client_reader, client_writer):
        """
        This method actually does the work to handle the requests for
        a specific client.  The protocol is line oriented, so there is
        a main loop that reads a line with a request and then sends
        out one or more lines back to the client with the result.
        """
        while True:
            #data = (yield from client_reader.readline()).decode("utf-8")
            _data = (yield from client_reader.read(1024)).decode("utf-8")
            if not _data:
                break

            print(_data)
            for item in _data.rstrip().split('\n'):
                if not item: # an empty string means the client disconnected
                    break
                data = item.rstrip()

                if data[13:17] == 'BP05' and data[32:35] != 'HSO':
                    imei = data[1:13]
                    print("Request for login from, ", imei)
                    print("Sending the login ACK to %s" % imei)
                    client_writer.write("({}AP05)".format(imei).encode("utf-8"))

                #Check for Handshake Packet
                elif data[13:17] == 'BP00' and data[32:35] == 'HSO':
                    imei = data[1:13]
                    print("Handshake request recieved from", imei)
                    print("Sending Handshake ACK to %s" % imei)
                    #client_writer.write("({}AP01HSO)".format(imei).encode("utf-8"))

                #Check For Alarm Packet
                elif data[13:17] == 'BO01':
                    imei = data[1:13]
                    alarm_type = data[17]
                    print("Alarm received from", imei)
                    self.send('(' + self.imei + 'AS01' + self.alarm_type + ')')

                    # Breaking the rest of the Data

                #Check for Normal Packet
                elif data[13:17] == 'BR00':
                    imei = data[1:13]
                    print("Received Normal packet from", imei)

                    # Breaking the rest of the Data
                    p = Packet()
                    p.decode_packet(imei, 'DNRML' , data[17:])

                    Entry.objects.create(**{
                        'imei': p.imei,
                        'timestamp': p.packet_time,
                        'peername': client_writer.get_extra_info('peername')[0],
                        'longitude': round(float(p.lng), 6),
                        'latitude': round(float(p.lat), 6),
                        'speed': float(p.speed),
                    })

                else:
                    print("Fell to undefined case")
                    print(data)

                yield from client_writer.drain()



    def start(self, loop):
        """
        Starts the TCP server, so that it listens on port 12345.
        For each client that connects, the accept_client method gets
        called.  This method runs the loop until the server sockets
        are ready to accept connections.
        """
        self.server = loop.run_until_complete(
            asyncio.streams.start_server(
                self._accept_client,
                self.host,
                self.port,
                loop=loop
            )
        )

    def stop(self, loop):
        """
        Stops the TCP server, i.e. closes the listening socket(s).
        This method runs the loop until the server sockets are closed.
        """
        if self.server is not None:
            self.server.close()
            loop.run_until_complete(self.server.wait_closed())
            self.server = None

def main():
    loop = asyncio.get_event_loop()

    # creates a server and starts listening to TCP connections
    server = GPSServer('0.0.0.0', '9999')
    server.start(loop)

    try:
        loop.run_forever()
    finally:
        server.stop(loop)
        loop.close()


if __name__ == '__main__':
    main()
