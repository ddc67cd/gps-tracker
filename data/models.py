from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver
from datetime import timedelta


# Create your models here.

class Entry(models.Model):
    imei = models.CharField(max_length=255)
    timestamp = models.DateTimeField()

    created = models.DateTimeField(auto_now_add=True)

    peername = models.CharField(max_length=255)

    longitude = models.FloatField()
    latitude = models.FloatField()
    speed = models.FloatField()
    track = models.ForeignKey('Track', db_index=True, null=True, blank=True)

    def save(self, *args, **kwargs):
        from constance import config

        print('save entry %s' % self.pk)
        try:
            # demonstration purpose only
            previous_entries = Entry.objects.order_by('timestamp').filter(imei=self.imei, timestamp__gte=self.timestamp-timedelta(seconds=config.ONOFF_DELAY)).exclude(pk=self.pk).exclude(track__isnull=True)[:1]
            print('Previous = %s' % previous_entries)

            track = previous_entries[0].track
        except IndexError:
            track = Track.objects.create()
        finally:
            print('track = %s' % track)
            self.track = track

        super().save(*args, **kwargs)

    def __str__(self):
        return '<RawData %s>' % self.pk

    class Meta:
        ordering = ['timestamp']


class Track(models.Model):
    def __str__(self):
        return '<Track %s>' % self.pk

#track_id = models.PositiveIntegerField(db_index=True, null=True, blank=True)

#@receiver(pre_save, sender=Entry, dispatch_uid="set_track_id")
