from django.shortcuts import render
from data.models import Entry


def index(request):
    return render(request, "index.html", {"entries": Entry.objects.all()})



from django.views.generic import ListView, DetailView
from data.models import Entry as RawData
from django.db.models import Min

class TracksListView(ListView):
    template_name = 'tracks-list.html'
    context_object_name = 'tracks'

    def get_queryset(self):
        return RawData.objects.values('track_id').annotate(created=Min('created')).order_by('track_id')


class TrackDetailView(DetailView):
    template_name = 'tracks-detail.html'
    context_object_name = 'track'

    def get_object(self):
        pk = self.kwargs.get(self.pk_url_kwarg)
        points = [list(x) for x in RawData.objects.filter(track_id=pk).values_list('latitude', 'longitude')]

        return {"points": points}
