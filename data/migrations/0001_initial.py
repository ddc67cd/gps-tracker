# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-02-08 18:44
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Entry',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('imei', models.CharField(max_length=255)),
                ('timestamp', models.DateTimeField()),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('peername', models.CharField(max_length=255)),
                ('longitude', models.FloatField()),
                ('latitude', models.FloatField()),
                ('speed', models.FloatField()),
            ],
        ),
    ]
